package org.acme.getting.started;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

@QuarkusTest
public class PetResourceTest {

    @Test
    void testGetPetsList() {
        given()
            .when().get("/pets")
            .then()
                .statusCode(200)
                .contentType(ContentType.JSON);
    }
}
