package org.acme.getting.started;

import org.acme.getting.started.pet.Pet;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.util.LinkedHashSet;
import java.util.Set;

@Path("/pets")
public class PetResource {
    private Set<Pet> pets = new LinkedHashSet<>();

    public PetResource() {
        pets.add(new Pet("Rudi", "cat", 2));
        pets.add(new Pet("Zina", "dog", 15));
    }

    @GET
    public Set<Pet> list() {
        return pets;
    }

    @POST
    public Set<Pet> add(Pet pet) {
        pets.add(pet);
        return pets;
    }

    @DELETE
    public Set<Pet> delete(Pet pet) {
        pets.removeIf(existingPet -> existingPet.name.contentEquals((pet.name)));
        return pets;
    }
}
