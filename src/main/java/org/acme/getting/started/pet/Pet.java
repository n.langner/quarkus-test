package org.acme.getting.started.pet;

public class Pet {
    public String name;
    public String species;
    public Integer age;

    public Pet() {
    }

    public Pet(String name, String species, Integer age) {
        this.name = name;
        this.species = species;
        this.age = age;
    }
}
