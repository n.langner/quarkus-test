#!/usr/bin/env python3

import requests
import os
import logging
import sys

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('sonar-metric-checker')

def get_env(variable, required=True):
    try:
        return os.environ[variable]
    except KeyError as e:
        if required:
            logger.critical('Required variable %s not provided', e, exc_info=True)
            sys.exit(1)
        else:
            logger.warning('Optional variable %s not provided. Using None instead', e)

class SonarMetricChecker:
    def __init__(self):
        self.sonar_url = get_env('SONAR_HOST_URL')
        self.sonar_token = get_env('SONAR_TOKEN')
        self.project_key = get_env('CI_PROJECT_NAME')
        self.branch = get_env('CI_COMMIT_BRANCH', False)
        self.mr_id = get_env('CI_MERGE_REQUEST_IID', False)

    def check(self):
        if self._check_credentials() == False:
            logger.critical('Credentials check failed. Exiting')
            sys.exit(1)
        self._check_quality_gates()
    
    def _check_credentials(self):
        uri = self.sonar_url + '/api/authentication/validate'
        logger.info('Checking Sonarqube credentials by calling %s', uri)
        sonar_request = requests.get(uri, auth=(self.sonar_token, ''))
        result = sonar_request.json()
        return result['valid'] == True
    
    def _check_quality_gates(self):
        qgates = self._get_quality_gates()
        uri = self._add_branch_or_mr(self.sonar_url + '/summary/new_code', True) + '&id=' + self.project_key
        status = qgates['projectStatus']['status']
        if status == 'ERROR':
            logger.critical('Quality Gates check failed. Refer to %s report for more details', uri)
            sys.exit(1)
        else:
            logger.info('Quality Gate check ended with status "%s" Refer to %s for more details', status, uri)
    
    def _get_quality_gates(self):
        url = '{}/api/qualitygates/project_status?projectKey={}'.format(self.sonar_url, self.project_key)
        url = self._add_branch_or_mr(url)
        logger.info('Getting Quality Gates from Sonarqube instance with URI %s', url)
        sonar_request = requests.get(url, auth=(self.sonar_token, ''))
        return sonar_request.json()
    
    def _add_branch_or_mr(self, url: str, first = False):
        separtator = '?' if first else '&'
        if self.branch:
            return url + '{}branch={}'.format(separtator, self.branch)
        elif self.mr_id:
            return url + '{}pullRequest={}'.format(separtator, self.mr_id)
        else:
            return url

if __name__ == '__main__':
    sonar_checker = SonarMetricChecker()
    sonar_checker.check()
